﻿using UnityEngine;
using System.Collections;

public class TankController1 : MonoBehaviour {

    public GameObject pc;

    public int speed = 5;
    public int fast_speed = 8;
    public int speedRotation = 5;

	
	void Start () {

        pc = (GameObject)this.gameObject;

	
	}
	
	
	void Update () {


        if (Input.GetKey(KeyCode.UpArrow))
        {
            pc.transform.position += pc.transform.forward * speed * Time.deltaTime;
        }


        if (Input.GetKey(KeyCode.DownArrow))
        {
            pc.transform.position -= pc.transform.forward * speed * Time.deltaTime;
          
        }


        if (Input.GetKey(KeyCode.LeftArrow))
        {
            pc.transform.Rotate(Vector3.down * speedRotation);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            pc.transform.Rotate(Vector3.up * speedRotation);
        } 




	}
}
